import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    html, body {
        margin: 0;
        padding: 0;
    }

    *, *::after, *::before {
        box-sizing: boder-box;
    }

    body {
        display: flex;
        height: 100vh;
        width: 100vw;
        background: ${({ theme }) => theme.colors.primaryDark};
        color: white;
        justify-content: center;
        align-items: center;
    }
`;
