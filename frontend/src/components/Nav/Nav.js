import { PropTypes } from 'prop-types';
import { StyledNav } from './Nav.styled';

function Nav({ open }) {
  return (
    <StyledNav open={open}>
      <a href="/">Our Mission</a>
      <a href="/">Our Values</a>
      <a href="/">Schedule</a>
      <a href="/">Current Standings</a>
    </StyledNav>
  );
}

Nav.propTypes = {
  open: PropTypes.bool.isRequired,
};

export default Nav;
