import { useState } from 'react';
import './App.css';
import { ThemeProvider } from 'styled-components';

import { GlobalStyles } from './GlobalStyles/global';
import { theme } from './theme/theme';

import Nav from './components/Nav';
import Burger from './components/Burger/Burger';

function App() {
  const [open, setOpen] = useState(false);
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyles />
        <div>
          <h1>This is a skeletal frontend.</h1>
          <h2>Please do not judge too harshly.</h2>
        </div>
        <div>
          <Burger open={open} setOpen={setOpen} />
          <Nav open={open} setOpen={setOpen} />
        </div>
      </>
    </ThemeProvider>
  );
}

export default App;
