export const theme = {
  colors: {
    primaryDark: '#0D0C1D',
    primaryHover: '#140554',
  },
  sizes: {
    mobile: '576px',
  },
};
