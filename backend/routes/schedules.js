const express = require('express');
const router = express.Router();
const Schedule = require('../models/schedule');

async function getSchedule(req, res, next) {
    let schedule;

    try {
        schedule = await Schedule.findById(req.params.id);
        if (schedule === null) {
            return res.status(404).json({ message: 'Cannot find schedule' });
        }
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }

    res.schedule = schedule;
    next();
}

// Get all schedules
router.get('/', async (req, res) => {
    try {
        const schedules = await Schedule.find();
        res.json(schedules);
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})
// Get a specific schedule
router.get('/:id', getSchedule, async (req, res) => {
    res.json(res.schedule);
})

// Post a new schedule
router.post('/', async (req, res) => {
    const schedule = await Schedule({
        date: req.body.date,
        photo: req.body.photo
    });

    try {
        const newSchedule = await schedule.save();
        res.status(201).json(newSchedule)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

router.put('/:id', getSchedule, async (req, res) => {
    res.schedule.date = req.body.date;
    res.schedule.picture = req.body.picture;
    
    try {
        const updatedSchedule = await res.schedule.save();
        res.json(updatedSchedule);
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

router.delete('/:id', getSchedule, async (req, res) => {
    try {
        await res.schedule.remove()
        res.send({ message: 'Schedule was deleted' });

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

module.exports = router;