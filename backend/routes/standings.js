const express = require('express');
const router = express.Router();
const Standing = require('../models/standings');

async function getStanding(req, res, next) {
    let standing;

    try {
        standing = await Standing.findById(req.params.id);
        if (standing === null) {
            return res.status(404).json({ message: 'Cannot find standing' });
        }
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }

    res.standing = standing;
    console.log("what is standing in getStanding?", res.standing);
    next();
}

// Get all Standings
router.get('/', async (req, res) => {
    try {
        const standings = await Standing.find();
        res.json(standings)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

// Get a specific Standing
router.get('/:id', getStanding, async (req, res) => {
    res.json(res.standing);
})

// Create a Standing
router.post('/', async (req, res) => {
    const standing = await Standing({
        teamName: req.body.teamName,
        wins: req.body.wins,
        losses: req.body.losses,
        pointsFor: req.body.pointsFor,
        pointsAgainst: req.body.pointsAgainst,
        pointDif: req.body.pointDif,
    });

    try {
        const newStanding = await standing.save();
        res.status(201).json(newStanding)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

// Update a Standing
router.put('/:id', getStanding, async (req, res) => {
    res.standing.teamName = req.body.teamName
    res.standing.wins = req.body.wins
    res.standing.losses = req.body.losses
    res.standing.pointsFor = req.body.pointsFor
    res.standing.pointsAgainst = req.body.pointsAgainst
    res.standing.pointDif = req.body.pointDif

    try {
        const updatedStanding = await res.standing.save();
        res.json(updatedStanding);
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

// Delete a Standing
router.delete('/:id', getStanding, async (req, res) => {
    try {
        await res.standing.remove()
        res.send({ message: 'Standing was deleted' });

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

module.exports = router