const express = require('express');
const router = express.Router();
const bcrypt = require("bcryptjs");
const Joi = require("@hapi/joi");
const jwt = require('jsonwebtoken');

const User = require("../models/user");

const registerSchema = Joi.object({
    username: Joi.string().min(6).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
})

const loginSchema = Joi.object({
    username: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
})

router.post("/register", async (req, res) => {
    const { error } = registerSchema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Check if user already exists
    const emailExists = await User.findOne({ email: req.body.email});
    if (emailExists) return res.status(400).send("Email already exists");

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: hashPassword,
    })

    try {
       const savedUser = await user.save();
       res.send(savedUser);
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

router.post("/login", async (req, res) => {
    const { error } = loginSchema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    console.log("body", req.body)

    const user = await User.findOne({ email: req.body.username});
    if (!user) return res.status(400).send("Email or password is wrong");

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send("Email or password is wrong");

    // Create and assign a token
    const token = jwt.sign( { _id: user._id }, process.env.TOKEN_SECRET);
    res.header("auth-token", token).send(token);

})

module.exports = router;