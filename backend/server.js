const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 3080;

const scheduleRouter = require('./routes/schedules');
const standingRouter = require('./routes/standings');
const authenticationRouter = require('./routes/authentication')

async function main() {
  mongoose.connect(process.env.MONGO_CONNECTION_STRING);
  const db = mongoose.connection
  db.on('error', error => console.log(error));
  db.once('open', () => console.log('Connected to Database'));

  app.use(express.json())
  app.use(cors());
  app.use('/user', authenticationRouter);
  app.use('/schedules', scheduleRouter);
  app.use('/standings', standingRouter);
  app.use((err, req, res, next) => {
    console.log('err:', JSON.stringify(err, null, 2));
    next(err);
  });

  app.listen(port, () => console.log('Hey look it works!'));
}

main().catch(err => console.log(err));