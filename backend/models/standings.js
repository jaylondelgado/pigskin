const mongoose = require('mongoose')

const standingSchema = new mongoose.Schema({
    teamName: {
        type: String,
        required: true,
    },

    wins: {
        type: Number,
        required: true,
    },

    losses: {
        type: Number,
        required: true,
    },

    pointsFor: {
        type: Number,
        required: true,
    },

    pointsAgainst: {
        type: Number,
        required: true,
    },

    pointDif: {
        type: Number,
        required:  true,
    }
})

const Standing = mongoose.model('Standing', standingSchema);

module.exports = Standing