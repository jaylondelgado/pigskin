const mongoose = require('mongoose');

const scheduleSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
    },

    photo: {
        type: String,
        required: true,
    }
})

const Schedule = mongoose.model('Schedule', scheduleSchema);

module.exports = Schedule;